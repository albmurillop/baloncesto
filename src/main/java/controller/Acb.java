package controller;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.ModeloDatos;

public class Acb extends HttpServlet {

	private static final String BOTON_VOTAR = "B1";
	private static final String BOTON_REINCIAR_VOTOS = "reiniciarVotos";
	private static final String CAMPO_NOMBRE_VISITANTE = "txtNombre";
	private static final String CAMPO_NOMBRE_JUGADOR = "R1";
	private static final String CAMPO_OTROS = "txtOtros";
	private static final String VALOR_OTROS = "Otros";
	private static final String NOMBRE_CLIENTE = "nombreCliente";
	private static final String JUGADORES = "jugadores";
	private static final String PAGINA_TABLA_VOTOS = "TablaVotos.jsp";
	private static final String PAGINA_VOTOS_REINICIADOS = "votos-reiniciados.html";
	private static final String PAGINA_VER_VOTOS = "VerVotos.jsp";

	private ModeloDatos bd;

	@Override
	public void init(ServletConfig configuracion) throws ServletException {
		bd = new ModeloDatos();
		bd.abrirConexion();
	}

	@Override
	public void service(HttpServletRequest peticion, HttpServletResponse respuesta)
			throws ServletException, IOException {
		if (peticion.getParameter(BOTON_VOTAR) != null) {
			votar(peticion, respuesta);
		} else if (peticion.getParameter(BOTON_REINCIAR_VOTOS) != null) {
			reiniciarVotos(respuesta);
		} else {
			verVotos(peticion, respuesta);
		}
	}

	private void votar(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException {
		HttpSession sesion = peticion.getSession(true);
		String nombreVisitante = peticion.getParameter(CAMPO_NOMBRE_VISITANTE);
		String nombreJugador = peticion.getParameter(CAMPO_NOMBRE_JUGADOR);
		if (nombreJugador.equals(VALOR_OTROS)) {
			nombreJugador = peticion.getParameter(CAMPO_OTROS);
		}

		if (bd.existeJugador(nombreJugador)) {
			bd.actualizarJugador(nombreJugador);
		} else {
			bd.insertarJugador(nombreJugador);
		}

		sesion.setAttribute(NOMBRE_CLIENTE, nombreVisitante);
		respuesta.sendRedirect(respuesta.encodeRedirectURL(PAGINA_TABLA_VOTOS));
	}

	private void reiniciarVotos(HttpServletResponse respuesta) throws IOException {
		bd.reiniciarJugadores();
		respuesta.sendRedirect(respuesta.encodeRedirectURL(PAGINA_VOTOS_REINICIADOS));
	}

	private void verVotos(HttpServletRequest peticion, HttpServletResponse respuesta) throws IOException {
		HttpSession sesion = peticion.getSession(true);
		sesion.setAttribute(JUGADORES, bd.obtenerJugadores());
		respuesta.sendRedirect(respuesta.encodeRedirectURL(PAGINA_VER_VOTOS));
	}

	@Override
	public void destroy() {
		bd.cerrarConexion();
		super.destroy();
	}
}
