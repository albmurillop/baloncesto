package domain;
public class Jugador {

	private String nombre;

	private Integer votos;

	public Jugador(String nombre, Integer votos) {
		this.nombre = nombre;
		this.votos = votos;
	}

	public String getNombre() {
		return nombre;
	}

	public Integer getVotos() {
		return votos;
	}
}
