package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import domain.Jugador;

public class ModeloDatos {

	private static final Logger LOGGER = Logger.getLogger(ModeloDatos.class.getName());
	private static final String ERROR = "El error es: %s";

	private Connection con;
	private Statement set;
	private ResultSet rs;

	public void abrirConexion() {

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			// Con variables de entorno
			String dbHost = System.getenv().get("DATABASE_HOST");
			String dbPort = System.getenv().get("DATABASE_PORT");
			String dbName = System.getenv().get("DATABASE_NAME");
			String dbUser = System.getenv().get("DATABASE_USER");
			String dbPass = System.getenv().get("DATABASE_PASS");

			String url = dbHost + ":" + dbPort + "/" + dbName;
			con = DriverManager.getConnection(url, dbUser, dbPass);

		} catch (Exception e) {
			// No se ha conectado
			LOGGER.log(Level.SEVERE, "No se ha podido conectar");
			LOGGER.log(Level.SEVERE, String.format(ERROR, e.getMessage()));
		}
	}

	public List<Jugador> obtenerJugadores() {
		List<Jugador> jugadores = new ArrayList<>();
		try {
			set = con.createStatement();
			rs = set.executeQuery("SELECT * FROM Jugadores");
			while (rs.next()) {
				jugadores.add(new Jugador(rs.getString("nombre"), rs.getInt("votos")));
			}

			rs.close();
			set.close();
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, String.format(ERROR, e.getMessage()));
		}

		return jugadores;
	}

	public boolean existeJugador(String nombre) {
		boolean existe = false;
		String cad;
		try {
			set = con.createStatement();
			rs = set.executeQuery("SELECT * FROM Jugadores");
			while (rs.next()) {
				cad = rs.getString("Nombre");
				cad = cad.trim();
				if (cad.compareTo(nombre.trim()) == 0) {
					existe = true;
				}
			}
			rs.close();
			set.close();
		} catch (Exception e) {
			// No lee de la tabla
			LOGGER.log(Level.SEVERE, "No lee de la tabla");
			LOGGER.log(Level.SEVERE, String.format(ERROR, e.getMessage()));
		}
		return (existe);
	}

	public Integer votosJugador(String nombre) {
		Integer votos = null;
		try {
			set = con.createStatement();
			rs = set.executeQuery("SELECT votos FROM Jugadores WHERE nombre " + " LIKE '%" + nombre + "%'");
			while (rs.next()) {
				votos = rs.getInt("votos");
			}
			rs.close();
			set.close();
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, String.format(ERROR, e.getMessage()));
		}

		return votos;
	}

	public void actualizarJugador(String nombre) {
		try {
			set = con.createStatement();
			set.executeUpdate("UPDATE Jugadores SET votos=votos+1 WHERE nombre " + " LIKE '%" + nombre + "%'");
			rs.close();
			set.close();
		} catch (Exception e) {
			// No modifica la tabla
			LOGGER.log(Level.SEVERE, "No modifica la tabla");
			LOGGER.log(Level.SEVERE, String.format(ERROR, e.getMessage()));
		}
	}

	public void insertarJugador(String nombre) {
		try {
			set = con.createStatement();
			set.executeUpdate("INSERT INTO Jugadores " + " (nombre,votos) VALUES ('" + nombre + "',1)");
			rs.close();
			set.close();
		} catch (Exception e) {
			// No inserta en la tabla
			LOGGER.log(Level.SEVERE, "No inserta en la tabla");
			LOGGER.log(Level.SEVERE, String.format(ERROR, e.getMessage()));
		}
	}

	public void reiniciarJugadores() {
		try {
			set = con.createStatement();
			set.executeUpdate("UPDATE Jugadores SET votos = 0");
			rs.close();
			set.close();
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, String.format(ERROR, e.getMessage()));
		}
	}

	public void cerrarConexion() {
		try {
			con.close();
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, String.format(ERROR, e.getMessage()));
		}
	}
}
