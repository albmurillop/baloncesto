<!DOCTYPE html>
<html lang="es">

<head>
    <title>Votaci&oacute;n mejor jugador liga ACB</title>
    <link href="estilos.css" rel="stylesheet" type="text/css" />
</head>

<body class="resultado">
    <p class="fuente-30">
        Votaci&oacute;n al mejor jugador de la liga ACB
    </p>
    <hr>
    <p class="fuente-30">
        <% String nombreP=(String) session.getAttribute("nombreCliente"); %>
        Muchas gracias <%=nombreP%> por su voto
    </p>
    <br>
    <br> <a href="index.html"> Ir al comienzo</a>
</body>

</html>