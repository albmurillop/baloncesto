<%@page import="java.util.List"%>
<%@page import="domain.Jugador"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Ver votos de los jugadores de la ACB</title>
    <link href="estilos.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <%
    	List<Jugador> jugadores = (List<Jugador>) session.getAttribute("jugadores");
    %>
    <h1 class="texto-centrado">VOTOS</h1>
    <hr>

    <table>
        <thead>
            <tr>
                <th id="nombreJugador">Nombre</th>
                <th id="votosJugador">Votos</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${jugadores}" var="jugador" varStatus="status">
                <tr>
                    <td>${jugador.nombre}</td>
                    <td>${jugador.votos}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>

    <br />
    <a class="blanco" href="index.html">P&aacute;gina principal</a>

</body>
</html>