import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

import model.ModeloDatos;

public class ModeloDatosTest {
	
	@Test
	public void testExisteJugador() {
		System.out.println("Prueba de existeJugador");
		String nombre = "";
		ModeloDatos instance = new ModeloDatos();
		boolean expResult = false;
		boolean result = instance.existeJugador(nombre);
		assertEquals(expResult, result);
//		fail("Fallo forzado.");
	}

	@Test
	public void testDadoQueSeHaVotadoPorUnJugadorSeHaIncrementadoEn1SusVotos() {
		ModeloDatos bd = mock(ModeloDatos.class);
		String nombreJugador = "Nombre";
		Integer votosIniciales = 0;
		Integer votosEsperados = 1;
		
		when(bd.votosJugador(nombreJugador)).thenReturn(votosIniciales);
		assertEquals(bd.votosJugador(nombreJugador), votosIniciales);

		bd.actualizarJugador(nombreJugador);

		when(bd.votosJugador(nombreJugador)).thenReturn(votosEsperados);
		assertEquals(bd.votosJugador(nombreJugador), votosEsperados);
	}
}
