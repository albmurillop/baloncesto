import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

public class PruebasPhantomjsIT {
	
	private static final String URL = "http://localhost:8080/Baloncesto/";

	private WebDriver driver;

	@BeforeEach
	public void setUp() {
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setJavascriptEnabled(true);
		caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
		caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
				new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
		driver = new PhantomJSDriver(caps);
	}

	@AfterEach
	public void tearDown() {
		driver.quit();
	}

	@Test
	public void tituloIndexTest() {
		driver.navigate().to(URL);
		assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(), "El titulo no es correcto");
		System.out.println(driver.getTitle());
		driver.close();
	}

	@Test
	public void dadoQueSePulsaBotonPonerVotosCeroEntoncesVotosSonCeroTest() {
		driver.navigate().to(URL);
		driver.findElement(By.name("reiniciarVotos")).click();
		driver.findElement(By.linkText("Página principal")).click();
		driver.findElement(By.name("verVotos")).click();
		for (int jugador = 1; jugador <= 4; jugador++) {
			String votos = "tr:nth-child(" + jugador + ") > td:nth-child(2)";
			assertEquals("0", driver.findElement(By.cssSelector(votos)).getText());
		}
		driver.close();
	}
	
	@Test
	public void dadoQueSeVotaPorOtroJugadorEntoncesTieneUnVotoTest() {
		driver.navigate().to(URL);
	    driver.findElement(By.name("txtOtros")).click();
	    driver.findElement(By.name("txtOtros")).sendKeys("Murillo");
	    driver.findElement(By.cssSelector("p:nth-child(12) > input:nth-child(1)")).click();
	    driver.findElement(By.name("B1")).click();
	    driver.findElement(By.linkText("Ir al comienzo")).click();
	    driver.findElement(By.name("verVotos")).click();
	    assertEquals("1", driver.findElement(By.cssSelector("tr:nth-child(5) > td:nth-child(2)")).getText());
	    driver.close();
	}
}
